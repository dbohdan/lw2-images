#! /usr/bin/env tclsh
package require Tcl 8.6
package require fileutil

apply {{} {
    set myDir [file dirname [file dirname [file normalize [info script]/___]]]
    lappend ::auto_path $myDir/lib $myDir/vendor
}}

package require lw2-images
package require jimhttp::json 2

proc wayback-url url {
    if {[regexp ^/ $url]} {
        set url http://lesswrong.com$url
    }
    set timestamp [clock format [clock seconds] -format %Y%m%d%H%M%S]
    return https://web.archive.org/web/$timestamp/$url
}

proc download-file {url dest} {
    file mkdir [file dirname $dest]
    log info "downloading [list $url] to [list $dest]"
    try {
        curl::curl -o $dest \
                   --location \
                   $url
    } trap {CURL HTTP 404} {} {
        log error {404 Not Found}
    } on error result {
        log error "failed with error [list $result]"
    }
}

proc main {argv0 argv} {
    namespace path ::trivial-log
    set delay 10

    foreach file $argv {
        log debug "processing file [list $file]"
        set json [fileutil::cat $file]
        foreach {_ post} [json::parse $json] {
            set id [dict get $post id]
            foreach {_ src} [dict get $post images] {
                set url [wayback-url $src]
                set dest results/images/$id/[file tail $src]
                if {[file exists $dest]} {
                    log warning "file [list $dest] exists; skipping"
                    continue
                }
                download-file $url $dest
                log debug "waiting $delay seconds"
                after [expr {$delay * 1000}]
            }
        }
    }
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    main $argv0 $argv
}
