namespace eval curl {
    proc curl args {
        try {
            exec curl \
                --fail \
                --silent \
                --show-error \
                {*}$args \
                2>@1
        } on error {result opts} {
            set errorcode CURL
            if {[regexp {returned error: ([0-9]+)} $result _ httpError]} {
                lappend errorcode HTTP $httpError
            }
            return -code error \
                   -errorcode $errorcode \
                   [lindex [split $result \n] 0]
        }
    }
}
