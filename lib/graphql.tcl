namespace eval graphql {
    variable endpoint https://www.lesserwrong.com/graphql
    variable tries 3

    proc request query {
        variable endpoint
        variable tries

        for {set i 0} {$i < $tries} {incr i} {
            try {
                set result [curl::curl \
                    --request POST \
                    --header Content-Type:application/graphql \
                    --data $query \
                    $endpoint]
                return [json::parse $result]
            } trap {CURL HTTP 503} {result opts} {}
        }
        return -options $opts $result
    }
}
