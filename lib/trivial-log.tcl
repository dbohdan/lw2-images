namespace eval trivial-log {
    proc log {level text} {
        set t [clock seconds]
        set timestamp [clock format $t -format {%Y-%m-%d %H:%M:%S} \
                                       -gmt 1]
        puts stderr "\[$timestamp\] \[$level\] $text"
    }
}
