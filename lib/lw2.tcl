namespace eval lw2 {
    namespace eval queries {
        proc posts-single id {
            format {{
                PostsSingle(documentId:"%1$s") {
                    title,
                    _id,
                    slug,
                    userId,
                    postedAt,
                    baseScore,
                    commentCount,
                    pageUrl,
                    url,
                    frontpageDate,
                    meta,
                    draft,
                    htmlBody
                }
            }} $id
        }

        proc posts-list {after before} {
            # You can't list more than 1000 posts at a time.
            format {{
                PostsList(terms:{limit: 1000, after:"%1$s", before:"%2$s"}) {
                    title,
                    _id,
                    userId,
                    postedAt,
                    htmlBody
                }
            }} $after $before
        }
    }

    proc post-images id {
        set data [graphql::request [queries::posts-single $id]]
        set html [dict get $data data PostsSingle htmlBody]
        scraper::image-urls $html
    }

    proc posts-list {after before} {
        set data [graphql::request [queries::posts-list $after $before]]
        dict get $data data PostsList
    }
}
