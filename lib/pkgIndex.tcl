package ifneeded lw2-images 0 [list apply {dir {
    foreach file {
        curl.tcl
        graphql.tcl
        lw2.tcl
        scraper.tcl
        trivial-log.tcl
    } {
        uplevel 1 source [file join $dir $file]
    }
    package provide lw2-images 0
}} $dir]
