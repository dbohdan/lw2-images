namespace eval scraper {
    proc image-urls html {
        try {
            set doc [::struct::tree]
            htmlparse::2tree $html $doc
            return [lmap {node type} [$doc attr type] {
                if {$type ne {img}} continue
                set attrs [$doc get $node data]
                # Ugly hack.
                if {![regexp -nocase {src="([^"]+?)"} $attrs _ src]} {
                    error "can't parse attributes [list $attrs]"
                }
                lindex $src
            }]
        } finally {
            catch {$doc destroy}
        }
    }
}