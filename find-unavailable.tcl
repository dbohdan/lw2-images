#! /usr/bin/env tclsh
package require Tcl 8.6
package require fileutil

apply {{} {
    set myDir [file dirname [file dirname [file normalize [info script]/___]]]
    lappend ::auto_path $myDir/lib $myDir/vendor
}}

package require lw2-images
package require jimhttp::json 2

proc available? {src delay} {
    if {![regexp {^https?} $src] ||
        [regexp lesswrong.com/static $src]} {
        return 0
    }
    try {
        log info "trying URL [list $src]"
        set content [curl::curl --location $src]
        if {[regexp -nocase {<!DOCTYPE html>} $content]} {
            return -code error -errorcode 404
        }
        log debug "waiting $delay seconds"
        after [expr {$delay * 1000}]
    } on ok {} {
        log info available
        return 1
    } trap CURL {} - trap 404 {} {
        log info unavailable
        return 0
    }
    error {this should never be reached}
}

proc main {argv0 argv} {
    namespace path ::trivial-log
    set delay 10

    foreach file $argv {
        log debug "processing file [list $file]"
        set json [fileutil::cat $file]
        set updated [lmap post [json::parse $json 0] {
            set unavailable [lmap src [dict get $post images] {
                if {[available? $src $delay]} continue
                lindex $src
            }]
            if {$unavailable eq {}} continue
            dict set post images $unavailable
            lindex $post
        }]
        if {$updated eq {}} continue

        set dest results/unavailable/[file tail $file]
        log info "writing [list $dest]"
        set json [json::stringify $updated \
                                  0 \
                                  {N* {id string
                                       title string
                                       images {N* string}}}]
        fileutil::writeFile $dest $json
    }
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    main $argv0 $argv
}
