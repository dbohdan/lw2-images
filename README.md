# lw2-images

This is a hack to recover the lost images from [LessWrong.com](https://lesswrong.com) through the [Wayback Machine](http://web.archive.org/). `scan-posts.tcl` retrieves the posts for a given time period using [Lesswrong2](https://github.com/Discordius/Lesswrong2)'s GraphQL API and scans them for `<img>` tags. For every post that contains images stored on LessWrong.com itself it saves the post's id and the images' `src` attribute in `results/<YYYY-MM>.json`. `download-images.tcl` takes the image sources in the JSON files given to it on the command line and tries to download them from the Wayback Machine. `find-unavailable.tcl` checks which image sources in the given JSON files are unavailable and writes them to `results/unavailable/<original-filename>.json`.


## Dependencies

* curl(1)
* Tcl 8.6
* Tcllib

### Debian/Ubuntu

```sh
sudo apt install curl tcl tcllib
```


## Usage

```sh
tclsh scan-posts.tcl [YYYY-MM [YYYY-MM]]
tclsh find-unavailable.tcl results/*.json
tclsh download-images.tcl results/unavailable/*.json
```


## License

MIT.


## Acknowledgments

The developer would like to thank **saturn** the source code to whose [lw2-viewer](https://github.com/kronusaturn/lw2-viewer) helped with understanding the Lesswrong2 API.
