#! /usr/bin/env tclsh
package require Tcl 8.6
package require fileutil
package require htmlparse
package require struct

apply {{} {
    set myDir [file dirname [file dirname [file normalize [info script]/___]]]
    lappend ::auto_path $myDir/lib $myDir/vendor
}}

package require lw2-images
package require jimhttp::json 2

proc days-in-month {year month} {
    set t [clock scan $year-$month-01 -format %Y-%m-%d]
    expr {([clock add $t 1 month] - $t) / (24*60*60)}
}

proc get-posts {year month} {
    set after [format %u-%02u-%02u $year $month 1]
    set before [format %u-%02u-%02u $year \
                                    $month \
                                    [days-in-month $year $month]]
    set posts [lw2::posts-list $after $before]
    if {[llength $posts] >= 1000} {
        error "reached 1000 posts for interval $after - $before;\
               likely missing posts"
    }
    return $posts
}

proc filter-posts posts {
    lmap {i post} $posts {
        set html [dict get $post htmlBody]
        set images [lmap url [scraper::image-urls $html] {
            if {([regexp {^https?} $url] &&
                 ![regexp {lesswrong\.com} $url]) ||
                [regexp {^data:} $url]} continue
            lindex $url
        }]
        if {$images eq {}} continue
        dict create \
            id [dict get $post _id] \
            title [dict get $post title] \
            images $images
    }
}

proc parse-date-input ym {
    if {[regexp {^([0-9]{4})(?:-([0-9][0-9]?))?$} $ym _ year month]} {
        if {![info exists month]} { set month 1 }
        set year [string trimleft $year 0]
        set month [string trimleft $month 0]
        if {12 < $month || $month < 1} {
            error "month must be between 1 and 12; got [list $month]"
        }
        return [list $year $month]
    } else {
        error "expected YYYY?-MM?, but got [list $ym]"
    }
}

proc parse-argv argv {
    lassign $argv start end
    if {$start eq {}} {
        set start 2005-01
    }
    if {$end eq {}} {
        set end [clock format [clock seconds] -format %Y-%m]
    }

    lassign [parse-date-input $start] startYear startMonth
    lassign [parse-date-input $end] endYear endMonth

    return [list $startYear $startMonth $endYear $endMonth]
}

proc generate-date-range {startYear startMonth endYear endMonth} {
    set year $startYear
    set month $startMonth
    set range {}
    while {$year < $endYear ||
           ($year == $endYear && $month <= $endMonth)} {
        lappend range $year $month
        incr month
        if {$month == 12} {
            set month 1
            incr year
        }
    }
    return $range
}

proc main {argv0 argv} {
    namespace path trivial-log

    set delay 30
    set monthsToProcess [generate-date-range {*}[parse-argv $argv]]

    foreach {year month} $monthsToProcess {
        log debug [format %u-%02u $year $month]
        set posts [get-posts $year $month]
        set filtered [filter-posts $posts]
        if {$filtered ne {}} {
            set file results/[format %u-%02u $year $month].json
            log info "writing $file"
            set json [json::stringify $filtered \
                                      0 \
                                      {N* {id string
                                           title string
                                           images {N* string}}}]
            fileutil::writeFile $file $json
        }
        log debug "waiting $delay seconds"
        after [expr {$delay * 1000}]
    }
}

# If this is the main script...
if {[info exists argv0] && ([file tail [info script]] eq [file tail $argv0])} {
    main $argv0 $argv
}
